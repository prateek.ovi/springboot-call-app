package com.prateek.springbootcallenabledapp;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Call;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.twilio.type.PhoneNumber;

import java.net.URI;

@SpringBootApplication
public class SpringBootCallEnabledAppApplication implements ApplicationRunner {

    private final static String SID_ACCOUNT = "ACd4930d68f7cfc1223fd116e7ba941b86";
    private final static String AUTH_ID = "4baf2f993dc0d03bcc7819be2d3d6aeb";
    private final static String FROM_NUMBER="+19072067287";
    private final static String TO_NUMBER =" +918439133227";


    static {
        Twilio.init(SID_ACCOUNT, AUTH_ID);
    }

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCallEnabledAppApplication.class, args);
	}

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Call.creator(new PhoneNumber(TO_NUMBER), new PhoneNumber(FROM_NUMBER),
                     new URI("http://demo.twilio.com/docs/voice.xml")).create();
    }
}
